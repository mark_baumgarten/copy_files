### What is this repository for? ###

This is a simple python script that given:

- a file with a list of absolute paths
- a destination folder (which can be absolute or relative)
 
 will copy each file in the file list into the destination folder while preserving the original files folder structure.
 
 Since the script uses the copy2 python function the script should preserve the original files meta data.

## Get started

Clone this project.
```
git clone https://mark_baumgarten@bitbucket.org/mark_baumgarten/copy_files.git
```

Create the output folder
```
mkdir /foo/bar/outputfolder
```

Modify the following two variables in the script to reflect your system:

```
.......
file_paths_file = 'found_files1.txt'
.......
copy_to_folder_path = 'copied_data'
......
```

Run the script
```
python copy_found_files.py
```
