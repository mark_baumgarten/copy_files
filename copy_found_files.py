import os
import shutil


# A file containing absolute paths to files we need to copy
file_paths_file = 'found_files1.txt'

# A folder where we want to copy files to while preserving
# source file folder structure
copy_to_folder_path = 'copied_data'

if not os.path.exists(file_paths_file):
    print("File %s does not exist - exiting" % file_paths_file)
    exit(2)

if not os.path.isdir(copy_to_folder_path):
    print("Folder %s does not exist - exiting" % copy_to_folder_path)
    exit(2)


with open(file_paths_file, 'rb') as f:
    for line in f:
        file_path = line.strip()
        if not os.path.exists(file_path):
            print("Warning: File no longer exists!: %s" % file_path)
            continue

        if os.path.isfile(file_path):
            source_dirname = os.path.dirname(file_path)
        
            # Create destination dir if not there
            destination_dir = os.path.join(copy_to_folder_path, source_dirname)
            if not os.path.exists(destination_dir):
                os.makedirs(destination_dir)

            # Copy the file
            basename = os.path.basename(file_path)
            destination_file_path = os.path.join(destination_dir, basename) 
            shutil.copy2(file_path, destination_file_path)
        else:
            print("Warning: %s is not a file" % file_path)





